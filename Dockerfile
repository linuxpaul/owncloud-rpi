FROM linuxpaul/php-rpi
MAINTAINER linuxpaul <linuxpaul@hotmail.de> 

ENV OWNCLOUD_DB_USER OC_DB_ADM
ENV OWNCLOUD_DB_PASSWORD OcDbPassWd
ENV OWNCLOUD_DB_NAME owncloud
ENV OWNCLOUD_DB_HOST mysql
ENV OWNCLOUD_ADMIN OC_Admin
ENV OWNCLOUD_ADMIN_PW OcAdPassWd

ENV DEBIAN_FRONTEND noninteractive

RUN apt-get update

RUN apt-get install -y --no-install-recommends nginx openssl bzip2 sudo; apt-get clean

COPY owncloud /etc/nginx/sites-available/

RUN ln -s ../sites-available/owncloud /etc/nginx/sites-enabled/owncloud

ADD https://download.owncloud.org/community/owncloud-9.1.3.tar.bz2 /var/www
RUN tar -C /var/www -xf /var/www/owncloud-9.1.3.tar.bz2 \
    && rm /var/www/owncloud-9.1.3.tar.bz2 \
    && chown -R www-data:www-data /var/www

COPY docker-entrypoint.sh /usr/local/bin/

ADD config.php /var/www/owncloud/config/config.php
RUN chown www-data:www-data /var/www/owncloud/config/config.php; \
    chmod 0640 /var/www/owncloud/config/config.php
    
VOLUME /var/www/owncloud/data
RUN chown www-data:www-data /var/www/owncloud/data

EXPOSE 80 443

ENTRYPOINT ["docker-entrypoint.sh"]

CMD ["nginx","-g","daemon off;"]
